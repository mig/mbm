Subject: <{event/value}>: <{event/broker}>@<{event/host}> does not seem to be alive

<{event/broker}>@<{event/host}> does not seem to be alive and kicking.

Please make sure things work as expected.
