Subject: <{event/value}>: thread usage of <{event/broker}>@<{event/host}>

FYI, the thread usage of the <{event/broker}> broker on <{event/host}>
is now back to normal.
