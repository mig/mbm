Subject: <{event/value}>: <{event/host}> is dead

<{event/host}> seems to be dead.

Please make sure the underlying problem is understood and has been reported.
