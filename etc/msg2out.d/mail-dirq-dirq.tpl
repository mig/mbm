Subject: <{event/value}>: too big <{event/path}> directory queue on <{event/host}>

There are too many (<{event/extra/value}>) entries in the <{event/path}>
directory queue on <{event/host}>.
