Subject: <{event/value}>: CPU usage of <{event/broker}>@<{event/host}>

FYI, the CPU usage of the <{event/broker}> broker on <{event/host}>
is now back to normal.
