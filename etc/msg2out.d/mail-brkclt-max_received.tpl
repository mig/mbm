Subject: <{event/value}>: too many <{client_name}> messages received by the <{event/broker}> brokers

There are too many (<{event/extra/value}> msg/sec) messages received by the <{event/broker}> brokers
for your client <{client_name}> (<{destination}>), see:
https://mig-graphite.cern.ch/r/brkclt/received/<{client_name}>

Please make sure that your producers are working correctly.
