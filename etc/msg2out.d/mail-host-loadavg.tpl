Subject: <{event/value}>: too high load average on <{event/host}>

The load average on <{event/host}> is too high: <{event/extra/value}>.

Please make sure that the host works as expected.
