Subject: <{event/value}>: too high cpu usage on <{event/host}>

The cpu usage on <{event/host}> is too high: <{event/extra/value}>.

Please make sure that the host works as expected.
