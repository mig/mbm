Subject: <{event/value}>: too high heap usage of <{event/broker}>@<{event/host}>

The heap usage of the <{event/broker}> broker on <{event/host}> is too high: <{event/extra/value}>.

Please make sure that the broker works as expected.
