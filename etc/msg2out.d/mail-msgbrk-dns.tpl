Subject: <{event/value}>: incorrect DNS for <{event/broker}>@<{event/host}>

<{event/host}> is not appearing correctly in Load-Balanced DNS:
<{event/text}>.

Please make sure that this is expected.
