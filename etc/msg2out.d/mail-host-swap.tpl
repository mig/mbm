Subject: <{event/value}>: too high swap usage on <{event/host}>

The swap usage on <{event/host}> is too high: <{event/extra/value}>.

Please make sure that the host works as expected.
