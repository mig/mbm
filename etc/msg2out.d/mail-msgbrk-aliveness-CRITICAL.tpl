Subject: <{event/value}>: <{event/broker}>@<{event/host}> is dead

<{event/broker}>@<{event/host}> seems to be dead.

Please make sure the underlying problem is understood and is being addressed.
