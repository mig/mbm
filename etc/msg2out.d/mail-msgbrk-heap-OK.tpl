Subject: <{event/value}>: heap usage of <{event/broker}>@<{event/host}>

FYI, the heap usage of the <{event/broker}> broker on <{event/host}>
is now back to normal.
