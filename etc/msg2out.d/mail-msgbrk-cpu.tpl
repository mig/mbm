Subject: <{event/value}>: too high CPU usage of <{event/broker}>@<{event/host}>

The CPU usage of the <{event/broker}> broker on <{event/host}> is too high: <{event/extra/value}>.

Please make sure that the broker works as expected.
