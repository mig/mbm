Subject: <{event/value}>: too many threads used by <{event/broker}>@<{event/host}>

There are too many threads used by the <{event/broker}> broker on <{event/host}>: <{event/extra/value}>.

Please make sure that the broker works as expected.
