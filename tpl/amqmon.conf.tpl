#
# amqmon daemon configuration
#

daemon = true
loop = true
interval = 60
sleep = 1
queue = <{mbm/spooldir}>/events

<broker>
<<include amqmon.d/*.conf>>
</broker>
