#
# logparse configuration (ActiveMQ broker)
#

<file>
  group = msgbrk
  broker = <{name}>
  name = jsw
  path = <{logdir}>/wrapper.log
</file>

<file>
  group = msgbrk
  broker = <{name}>
  name = activemq
  path = <{logdir}>/activemq.log
  type = log4j
</file>

<file>
  group = msgbrk
  broker = <{name}>
  name = httpd-access
  path = /var/log/httpd/mb-<{name}>-jmx4perl-access.log
</file>

<file>
  group = msgbrk
  broker = <{name}>
  name = httpd-error
  path = /var/log/httpd/mb-<{name}>-jmx4perl-error.log
</file>

<file>
  group = msgbrk
  broker = <{name}>
  name = httpd-access
  path = /var/log/httpd/mb-<{name}>-webconsole-access.log
</file>

<file>
  group = msgbrk
  broker = <{name}>
  name = httpd-error
  path = /var/log/httpd/mb-<{name}>-webconsole-error.log
</file>
