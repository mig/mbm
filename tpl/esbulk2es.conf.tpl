#
# esbulk2es daemon configuration
#

daemon = true
loop = true
url = <{mbm/elasticsearch/url}>
auth = "<{mbm/elasticsearch/auth}>"

<queue>
  bulk = <{mbm/spooldir}>/esbulk
  rejected = <{mbm/spooldir}>/rejected
</queue>
