#
# metric2influxdb daemon configuration
#

daemon = true
loop = true
url = <{mbm/influxdb/url}>
auth = "<{mbm/influxdb/auth}>"

<queue>
  message = <{mbm/spooldir}>/<{id}>
  rejected = <{mbm/spooldir}>/rejected
</queue>
