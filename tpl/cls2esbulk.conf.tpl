#
# cls2esbulk daemon configuration
#

daemon = true
loop = true

<mapping>
  /topic/mig.evt.check    = mig_check_{day}
  /topic/mig.evt.log      = mig_log_{day}
  /topic/metis.evt.check  = mig_check_{day}
  /topic/metis.evt.status = mig_status_{year}
</mapping>

<queue>
  bulk = <{mbm/spooldir}>/esbulk
  message = <{mbm/spooldir}>/<{id}>
  rejected = <{mbm/spooldir}>/rejected
</queue>
