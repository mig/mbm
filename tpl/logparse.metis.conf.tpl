#
# logparse configuration (Metis)
#

<file>
  group = host
  name = metis
  path = <{mbm/metis/logdir}>/metis.log
  type = log4j
</file>
