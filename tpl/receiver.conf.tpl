#
# receiver daemon configuration (<{id}>/<{broker}>)
#

daemon = true
heart-beat = true
reliable = true

<incoming-broker>
  uri = "<{uri}>"
  auth = "<{auth}>"
  sockopts = "<{sockopts}>"
</incoming-broker>

<{subscribe}>

<outgoing-queue>
  path = "<{mbm/spooldir}>/<{id}>"
</outgoing-queue>
