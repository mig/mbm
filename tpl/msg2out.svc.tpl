#
# msg2out service configuration
#

<entry>
  var_name = msg2out
  var_pidfile = ${var_rundir}/${var_name}.pid
  var_program = ${var_bindir}/msg2out --pidfile ${var_pidfile}
  #
  type     = service
  name     = ${var_name}
  expected = running
  start    = ${var_program} --conf ${var_confdir}/${var_name}.conf
  stop     = ${var_program} --quit
  status   = ${var_program} --status
</entry>
