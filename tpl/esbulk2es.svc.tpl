#
# esbulk2es service configuration
#

<entry>
  var_name = esbulk2es<{id}>
  var_pidfile = ${var_rundir}/${var_name}.pid
  var_program = ${var_bindir}/esbulk2es --pidfile ${var_pidfile}
  #
  type     = service
  name     = ${var_name}
  expected = running
  start    = ${var_program} --conf ${var_confdir}/esbulk2es.conf
  stop     = ${var_program} --quit
  status   = ${var_program} --status
  timeout  = 90
</entry>
