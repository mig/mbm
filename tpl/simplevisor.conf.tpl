#
# mbm simplevisor configuration
#

<simplevisor>
    # the pidfile is set in the profile...
    log = syslog
    loglevel = info
    logname = mbm
    interval = 300
</simplevisor>

<entry>
    type = supervisor
    name = mbm
    window = 120
    adjustments = 60
    var_bindir = <{mbm/bindir}>
    var_confdir = <{mbm/confdir}>
    var_rundir = <{mbm/rundir}>
    var_spooldir = <{mbm/spooldir}>
    <children>
        <<include simplevisor.d/*.svc>>
    </children>
</entry>
