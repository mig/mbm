#
# sysmon configuration (Metis)
#

<process>
  _match = "java .+ -Dmetis\.base=<{mbm/metis/base}> .+ ch\.cern\.mig\.metis\.Main"
  group = metis
  path = <{mbm/metis/base}>
</process>

<dirq>
  path = <{mbm/metis/spooldir}>/incoming
</dirq>

<dirq>
  path = <{mbm/metis/spooldir}>/outgoing
</dirq>

<dirq>
  path = <{mbm/metis/spooldir}>/rejected
</dirq>
