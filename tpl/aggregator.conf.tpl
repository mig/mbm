#
# aggregator daemon configuration
#

daemon = true
loop = true
destination = /topic/<{mbm/destination}>

<queue>
  event = <{mbm/spooldir}>/events
  message = <{mbm/spooldir}>/messages
  rejected = <{mbm/spooldir}>/rejected
</queue>
