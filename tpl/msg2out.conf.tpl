#
# msg2out daemon configuration
#

daemon = true
loop = true

<queue>
  message = <{mbm/spooldir}>/<{id}>
  rejected = <{mbm/spooldir}>/rejected
</queue>

template = <{mbm/confdir}>/msg2out.d

<<include msg2out.d/*.conf>>
