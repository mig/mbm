#
# sender daemon configuration
#

daemon = true
loop = true
reliable = true
remove = true

<incoming-queue>
  path = "<{mbm/spooldir}>/messages"
  maxlock = 30
  maxtemp = 30
</incoming-queue>

<outgoing-broker>
  uri = "<{uri}>"
  auth = "<{auth}>"
  sockopts = "<{sockopts}>"
</outgoing-broker>
