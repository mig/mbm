#
# httpchk daemon configuration
#

daemon = true
loop = true
interval = 60
sleep = 1
queue = <{mbm/spooldir}>/events
threads = <{mbm/httpchecker/threads}>

<<include httpchk.d/*.conf>>
