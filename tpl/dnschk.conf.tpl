#
# dnschk daemon configuration
#

daemon = true
loop = true
interval = 60
sleep = 1
queue = <{mbm/spooldir}>/events

<<include dnschk.d/*.conf>>
