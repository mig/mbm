#
# stompchk daemon configuration
#

daemon = true
loop = true
interval = 60
sleep = 1
queue = <{mbm/spooldir}>/events
destination = /{dtype}/monitor.test.{proto}
threads = <{mbm/stompchecker/threads}>

<<include stompchk.d/*.conf>>
