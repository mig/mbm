#
# metmon daemon configuration
#

daemon = true
loop = true
interval = 60
sleep = 1
queue = <{mbm/spooldir}>/events
path = <{mbm/metis/base}>
port = <{mbm/metis/port}>
