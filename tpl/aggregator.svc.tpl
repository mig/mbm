#
# aggregator service configuration
#

<entry>
  var_name = aggregator
  var_pidfile = ${var_rundir}/${var_name}.pid
  var_program = ${var_bindir}/evt2msg --pidfile ${var_pidfile}
  #
  type     = service
  name     = ${var_name}
  expected = running
  start    = ${var_program} --conf ${var_confdir}/${var_name}.conf
  stop     = ${var_program} --quit
  status   = ${var_program} --status
</entry>
