#
# logparse daemon configuration
#

daemon = true
loop = true
skip = true
sleep = 1
memory = <{mbm/confdir}>/logparse.db
<{ifdef(mbm/logparser/callback)}>callback = <{mbm/confdir}>/<{mbm/logparser/callback}>
<{endif(mbm/logparser/callback)}>
<queue>
  path = <{mbm/spooldir}>/events
</queue>

<<include logparse.d/*.conf>>
