#
# receiver service configuration (<{id}>/<{broker}>)
#

<entry>
  var_pidfile = ${var_rundir}/<{id}>-<{broker}>.pid
  var_program = /usr/bin/stompclt --pidfile ${var_pidfile}
  #
  type     = service
  name     = <{broker}>
  expected = running
  start    = ${var_program} --conf ${var_confdir}/<{id}>-<{broker}>.conf 
  stop     = ${var_program} --quit
  status   = ${var_program} --status --timeout-status 5
  timeout  = 90
</entry>
