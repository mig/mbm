#
# metric2carbon daemon configuration
#

daemon = true
loop = true
host = <{mbm/carbon/host}>
port = <{mbm/carbon/port}>

<queue>
  message = <{mbm/spooldir}>/<{id}>
  rejected = <{mbm/spooldir}>/rejected
</queue>
