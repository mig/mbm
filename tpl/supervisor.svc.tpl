#
# supervisor configuration (<{id}>)
#

<entry>
    type = supervisor
    name = <{id}>
    <children>
        <<include <{id}>/*.svc>>
    </children>
</entry>
