#!/usr/bin/perl
#+##############################################################################
#                                                                              #
# File: stompchk                                                               #
#                                                                              #
# Description: probe message brokers via STOMP and queue monitoring events     #
#                                                                              #
#-##############################################################################

#
# used modules
#

use strict;
use warnings qw(FATAL all);
use threads;
use threads::shared;
use Authen::Credential qw();
use Config::Validator qw(is_true listof);
use Directory::Queue qw();
use IO::Socket::SSL qw(SSL_VERIFY_PEER);
use JSON qw();
use List::Util qw(shuffle sum);
use Messaging::Message qw();
use MBM qw(%Option %Schema $Filter);
use MBM::Event qw();
use Net::STOMP::Client qw();
use No::Worries qw($ProgramName);
use No::Worries::Die qw(handler dief);
use No::Worries::Log qw(log_debug);
use No::Worries::String qw(string_quantify);
use No::Worries::Warn qw(handler warnf);
use Thread::Queue qw();
use Time::HiRes qw();

#
# constants
#

use constant MAX_LATENCY => 5;
use constant THREAD_OPTIONS => { void => 1, stack_size => 64 * 1024 };

#
# global variables
#

our($EventQueue, $JSON, $ProbeId, $WorkQueue, @Work, $QThread);

#+##############################################################################
#                                                                              #
# configuration schema                                                         #
#                                                                              #
#-##############################################################################

$Schema{config} = {
    type => "struct",
    fields => {
        destination => { type => "string",              optional => "true"  },
        dtypes      => { type => "string",              optional => "false" },
        interval    => { type => "number",              optional => "false" },
        queue       => { type => "string",              optional => "false" },
        threads     => { type => "integer",             optional => "false" },
        # below are options only for the configuration file...
        _test       => { type => "list\?(valid(test))", optional => "false" },
    },
};

$Schema{test} = {
    type => "struct",
    fields => {
        auth        => { type => "valid(auth)",  optional => "true"  },
        broker      => { type => "string",       optional => "false" },
        destination => { type => "string",       optional => "true"  },
        host        => { type => "hostname",     optional => "false" },
        port        => { type => "integer",      optional => "false" },
        ssl         => { type => "boolean",      optional => "true"  },
        stomp       => { type => "valid(stomp)", optional => "true"  },
        timeout     => { type => "number",       optional => "true"  },
    },
};

$Schema{stomp} = {
    type => "struct",
    fields => {
        host    => { type => "string", optional => "true" },
        version => { type => "string", optional => "true" },
    },
};

$Schema{auth} = { type => "isa(Authen::Credential)" };

#+##############################################################################
#                                                                              #
# queue worker: put the generated events in the directory queue                #
#                                                                              #
#-##############################################################################

#
# queue the events spooled in memory
#

sub queue_handler ($$) {
    my($eq, $events) = @_;
    my($count, $name);

    $count = @{ $events };
    return unless $count;
    $name = $eq->add($JSON->encode([ { proto  => "tcp4" }, @{ $events } ]));
    log_debug("enqueued %s as %s", string_quantify($count, "event"), $name);
    @{ $events } = ();
}

#
# queue worker
#

sub queue_worker () {
    my($running, $limit, $eq, $event, @list);

    $eq = Directory::Queue->new(
        path => $Option{queue},
        type => "Simple",
    );
    local $SIG{TERM} = sub { $running = 0 };
    $limit = Time::HiRes::time() + MAX_LATENCY;
    $running = 1;
    log_debug("started queue_worker(%s)", $eq->path());
    while ($running) {
        $event = $EventQueue->dequeue_nb();
        if ($event) {
            # real event
            log_debug("received event %s", $JSON->encode($event))
                if $Option{debug} > 1;
            push(@list, $event) unless $Filter and not $Filter->($event);
        } elsif (defined($event)) {
            # clean end signaled from main
            last;
        } else {
            # nothing
            sleep(1);
        }
        if (@list >= $Option{batch} or Time::HiRes::time() >= $limit) {
            queue_handler($eq, \@list);
            $limit = Time::HiRes::time() + MAX_LATENCY;
        }
    }
    queue_handler($eq, \@list);
    log_debug("finished queue_worker()");
}

#+##############################################################################
#                                                                              #
# STOMP worker: test connectivity and response time                            #
#                                                                              #
#-##############################################################################

#
# prepare one STOMP test to be executed later (multiple times)
#

sub stomp_prepare ($) {
    my($test) = @_;
    my($auth, $uri, $destination, @dtypes, %map, %common, %work);

    $auth = $test->{auth} || Authen::Credential->parse("none");
    delete($test->{ssl}) if $test->{ssl} and not is_true($test->{ssl});
    $test->{proto} = $test->{ssl} ? "stomp+ssl" : "stomp";
    $uri = sprintf("%s://%s:%d", $test->{proto}, $test->{host}, $test->{port});
    $destination = $test->{destination} || $Option{destination};
    dief("missing destination to test %s", $uri) unless $destination;
    if ($destination =~ m{^/(dsub|queue|topic)/}) {
        @dtypes = ($1);
    } elsif ($destination =~ m{^/\{dtype\}/}) {
        @dtypes = split(/\W+/, $Option{dtypes});
    } else {
        dief("unexpected destination: %s", $destination);
    }
    %map = (
        host   => $test->{host},
        broker => $test->{broker},
        proto  => $test->{proto},
        port   => $test->{port},
    );
    foreach my $name (keys(%map)) {
        $map{$name} =~ s/[^0-9a-zA-Z\-]/_/g;
    }
    $test->{timeout} ||= 30;
    $test->{new} = {
        auth    => $auth,
        uri     => $uri,
        timeout => $test->{timeout},
    };
    $test->{new}{sockopts}{SSL_verify_mode} = SSL_VERIFY_PEER
        if $test->{ssl};
    $test->{connect} = {};
    if ($auth->scheme() eq "plain") {
        $test->{connect}{login} = $auth->name();
        $test->{connect}{passcode} = $auth->pass();
    }
    if ($test->{stomp}) {
        $test->{connect}{host} = $test->{stomp}{host}
            if $test->{stomp}{host};
        $test->{new}{accept_version} = $test->{stomp}{version}
            if $test->{stomp}{version};
    }
    %common = (
        host   => $test->{host},
        broker => $test->{broker},
        port   => $test->{port},
    );
    foreach my $dtype (@dtypes) {
        %work = %{ $test };
        $work{destination} = $destination;
        $map{dtype} = $dtype;
        foreach my $name (keys(%map)) {
            $work{destination} =~ s/\{$name\}/$map{$name}/g;
        }
        $work{check} = { %common };
        $work{check}{type} = "check";
        $work{check}{group} = "msgsvc";
        $work{check}{name} = "stomp-$dtype";
        $work{metric} = { %common };
        $work{metric}{type} = "metric";
        $work{metric}{group} = "msgchk";
        $work{metric}{name} = "elapsed";
        $work{metric}{check} = $work{check}{name};
        push(@Work, { %work });
    }
}

#
# STOMP topic tester
#

sub stomp_test_topic ($$$) {
    my($broker, $msg1, $timeout) = @_;
    my($time1, $time2, $frame, $tmp, $msg2);

    $time1 = Time::HiRes::time();
    $broker->send_frame(Net::STOMP::Client::Frame->demessagify($msg1));
    $frame = $broker->wait_for_frames(timeout => $timeout);
    $time2 = Time::HiRes::time();
    dief("probe message not received!") unless $frame;
    $tmp = $frame->command();
    dief("unexpected frame received: command=%s", $tmp)
        unless $tmp eq "MESSAGE";
    $msg2 = $frame->messagify();
    $tmp = $msg2->header_field("x-uuid") || "<unset>";
    dief("unexpected message received: x-uuid=%s", $tmp)
        unless $tmp eq $msg1->header_field("x-uuid");
    return($time2 - $time1);
}

#
# STOMP queue tester
#

sub stomp_test_queue ($$$) {
    my($broker, $msg1, $timeout) = @_;
    my($time1, $time2, $frame, $tmp, $msg2, %seen);

    $msg1->header_field(persistent => "true");
    $msg1->header_field(receipt => $broker->uuid());
    $time1 = Time::HiRes::time();
    $broker->send_frame(Net::STOMP::Client::Frame->demessagify($msg1));
    while (not $seen{MESSAGE} or not $seen{RECEIPT}) {
        $frame = $broker->wait_for_frames(timeout => $timeout);
        $time2 = Time::HiRes::time();
        dief("probe message not received!") unless $frame;
        $tmp = $frame->command();
        $seen{$tmp}++;
        if ($tmp eq "MESSAGE") {
            $msg2 = $frame->messagify();
            $tmp = $msg2->header_field("x-uuid") || "<unset>";
            dief("unexpected message received: x-uuid=%s", $tmp)
                unless $tmp eq $msg1->header_field("x-uuid");
        } elsif ($tmp eq "RECEIPT") {
            $tmp = $frame->header("receipt-id") || "<unset>";
            dief("unexpected receipt received: receipt-id=%s", $tmp)
                unless $tmp eq $msg1->header_field("receipt");
        } else {
            dief("unexpected frame received: command=%s", $tmp);
        }
    }
    return($time2 - $time1);
}

#
# STOMP tester
#

sub stomp_test ($) {
    my($test) = @_;
    my($broker, $peer, $msg, $frame, $receipt, $elapsed, $ts);

    # broker connection
    $broker = Net::STOMP::Client->new(%{ $test->{new} });
    # STOMP connection
    $broker->connect(%{ $test->{connect} });
    # so far so good
    $peer = $broker->peer();
    log_debug("connected to %s (%s) port %d using STOMP %s",
              $peer->host(), $peer->addr(), $peer->port(), $broker->version());
    # message preparation
    $msg = Messaging::Message->new(body => "A" x 1024);
    $msg->header_field("destination" => $test->{destination});
    $msg->header_field("x-type" => $ProbeId);
    $msg->header_field("x-uuid" => $broker->uuid());
    # subscribe (with receipt)
    $broker->message_callback(sub { return(1) });
    $broker->subscribe(
        destination => $test->{destination},
        receipt     => $broker->uuid(),
        id          => $broker->uuid(),
    );
    foreach my $attempt (1 .. int($test->{timeout})) {
        $frame = $broker->wait_for_frames(timeout => 1);
        if ($frame and $frame->command() eq "RECEIPT") {
            $receipt = $frame;
            last;
        }
    }
    dief("no subscription receipt received!") unless $receipt;
    # drain
    while (1) {
        $frame = $broker->wait_for_frames(timeout => 1);
        last unless $frame;
    }
    # send and receive
    if ($test->{destination} =~ /^\/topic\//) {
        # topic style
        $elapsed = stomp_test_topic($broker, $msg, $test->{timeout});
    } else {
        # queue style
        $elapsed = stomp_test_queue($broker, $msg, $test->{timeout});
    }
    $ts = Time::HiRes::time();
    log_debug("destination=%s elapsed=%.6f", $test->{destination}, $elapsed);
    # wait for (error) frames and disconnect
    $broker->wait_for_frames(timeout => 0.5);
    $broker->disconnect();
    # so far so good, we can record the elapsed time
    ## no critic 'ProhibitCommaSeparatedStatements'
    $EventQueue->enqueue({
        %{ $test->{metric} },
        ts    => $ts,
        value => $elapsed,
    });
}

#
# STOMP worker
#

sub stomp_worker () {
    my($running, $index, $error);

    local $SIG{TERM} = sub { $running = 0 };
    $running = 1;
    log_debug("started stomp_worker()");
    while ($running) {
        $index = $WorkQueue->dequeue_nb();
        if (not defined($index)) {
            # nothing
            sleep(1);
        } elsif ($index >= 0) {
            # real work
            eval { stomp_test($Work[$index]) };
            $error = $@;
            if ($error) {
                $error =~ s/\s+$//;
                ## no critic 'ProhibitCommaSeparatedStatements'
                $EventQueue->enqueue({
                    %{ $Work[$index]{check} },
                    ts    => Time::HiRes::time(),
                    value => MBM::Event::Check::VAL_CRITICAL,
                    text  => $error,
                });
            } else {
                ## no critic 'ProhibitCommaSeparatedStatements'
                $EventQueue->enqueue({
                    %{ $Work[$index]{check} },
                    ts    => Time::HiRes::time(),
                    value => MBM::Event::Check::VAL_OK,
                });
            }
        } else {
            # clean end signaled from main
            $running = 0;
        }
    }
    log_debug("finished stomp_worker()");
}

#+##############################################################################
#                                                                              #
# scheduler: schedule work to be done in a uniform way                         #
#                                                                              #
#-##############################################################################

sub scheduler () {
    my($count, $sleep, $work, $running, $end, $psleep);

    $count = @Work;
    $sleep = $Option{interval} / $count * 0.99;
    $work = 0;
    local $SIG{TERM} = sub { $running = 0 };
    $running = 1;
    log_debug("started scheduler(%d)", $count);
    while ($running) {
        $WorkQueue->enqueue($work);
        $work = ($work + 1) % $count;
        if ($sleep > 1) {
            $end = Time::HiRes::time() + $sleep;
            while ($running) {
                $psleep = $end - Time::HiRes::time();
                last if $psleep <= 0;
                $psleep = 1 if $psleep > 1;
                Time::HiRes::sleep($psleep);
            }
        } else {
            Time::HiRes::sleep($sleep);
        }
    }
    log_debug("finished scheduler()");
}

#+##############################################################################
#                                                                              #
# main code                                                                    #
#                                                                              #
#-##############################################################################

#
# initialisation
#

sub init () {
    $| = 1;
    $JSON = JSON->new()->utf8(1);
    MBM::handle_options(interval => 60, threads => 1, dtypes => "queue topic");
    # setup
    $ProbeId = sprintf("%s-2.22", $ProgramName);
    $EventQueue = Thread::Queue->new();
    $WorkQueue = Thread::Queue->new();
    # prepare the work to be dispatched
    foreach my $test (listof($Option{test})) {
        stomp_prepare($test);
    }
    @Work = shuffle(@Work);
    # handle the daemon & pidfile options
    MBM::handle_daemon() if $Option{daemon};
    MBM::handle_pidfile() if $Option{pidfile};
    # start the threads
    $QThread = threads->create(THREAD_OPTIONS, \&queue_worker);
    foreach my $thread (1 .. $Option{threads}) {
        threads->create(THREAD_OPTIONS, \&stomp_worker);
    }
    threads->create(THREAD_OPTIONS, \&scheduler);
    # allow some time for the threads to start...
    sleep(1);
}

#
# do what must be done (one pass)
#

sub work () {
    my($dead);

    # detect dead threads
    foreach my $thread (threads->list(threads::joinable)) {
        warnf("joining unexpected dead thread: %d", $thread->tid());
        $thread->join();
        $dead++;
    }
    # commit suicide in case of dead threads
    kill("TERM", $$) if $dead;
    return(0);
}

#
# cleanup
#

sub cleanup () {
    $EventQueue->enqueue("");
    foreach my $thread (1 .. $Option{threads}) {
        $WorkQueue->enqueue(-1);
    }
    sleep(1);
    foreach my $thread (threads->list()) {
        next if $thread == $QThread;
        $thread->kill("TERM");
    }
    foreach my $thread (threads->list()) {
        $thread->join();
    }
}

#
# just do it
#

init();
MBM::main(\&work);
cleanup();

__END__

=head1 NAME

stompchk - probe message brokers via STOMP and queue monitoring events

=head1 SYNOPSIS

B<stompchk> [I<OPTIONS>]

=head1 DESCRIPTION

This program uses STOMP to probe one or more ActiveMQ brokers, sending and
receiving test messages. Both checks and metrics are generated and queued as
monitoring events.

=head1 OPTIONS

=over

=item B<--batch> I<INTEGER>

set the maximum number of monitoring events to put in one directory queue entry
(default: 100)

=item B<--config>, B<--conf> I<PATH>

use the given configuration file

=item B<--daemon>

detach B<stompchk> so that it becomes a daemon running in the background;
debug, warning and error messages will get sent to syslog

=item B<--debug>, B<-d>

show debugging information; this option can be repeated

=item B<--destination> I<STRING>

set the default destination to use for the tests

=item B<--dtypes> I<STRING>

set the destination types to use for the tests (default: "queue topic")

=item B<--filter> I<PATH>

use the given filter (Perl code in the file at the given path) on each
generated event

=item B<--help>, B<-h>, B<-?>

show some help

=item B<--interval> I<NUMBER>

for each configured thing to monitor, sleep during the given number of seconds
between checks; can be fractional (default: 60)

=item B<--linger> I<INTEGER>

set the maximum number of seconds given to the program to reply to the
B<--quit> or B<--status> option (default: 10)

=item B<--list>, B<-l>

show all the supported options

=item B<--loop>

loop to generate monitoring events instead of working only once

=item B<--manual>, B<-m>

show this manual

=item B<--pidfile> I<PATH>

use this pid file

=item B<--queue> I<PATH>

write the generated events into the L<Directory::Queue::Simple> queue at the
given path

=item B<--quit>

tell another instance of B<stompchk> (identified by its pid file, as specified
by the B<--pidfile> option) to quit

=item B<--sleep> I<NUMBER>

sleep during the given number of seconds between internal thread checking; can
be fractional

=item B<--status>

get the status of another instance of B<stompchk> (identified by its pid file,
as specified by the B<--pidfile> option); the exit code will be zero if the
instance is alive and non-zero otherwise

=item B<--threads> I<INTEGER>

set the number of threads to use to run the tests (default: 1)

=back

=head1 AUTHOR

Lionel Cons L<http://cern.ch/lionel.cons>

Copyright (C) CERN 2014-2018
