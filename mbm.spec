#
# Messaging Based Monitoring (MBM) spec file
#

%define real_version %((cat %{_sourcedir}/VERSION || cat %{_builddir}/VERSION || echo UNKNOWN) 2>/dev/null)
%global home_dir      /opt/mbm
%global bin_dir       %{home_dir}/bin
%global cfg_dir       %{home_dir}/etc
%global lib_dir       %{home_dir}/lib
%global tpl_dir       %{home_dir}/tpl
%global spool_dir     /var/spool/mbm
%global run_dir       /var/run/mbm

%if 0%{?fedora} >= 17 || 0%{?rhel} >= 7
%define with_systemd 1
%else
%define with_systemd 0
%endif

Name:		mbm
Version:	%{real_version}
Release:	1%{?dist}
Summary:	Messaging Based Monitoring
Group:		System
License:	ASL 2.0
URL:		https://gitlab.cern.ch/mig/%{name}
Source0:	%{name}-%{version}.tgz
Source1:	VERSION
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:	noarch
%if %{with_systemd}
BuildRequires:	systemd
%endif
Requires:	python-simplevisor

%description
The purpose of the Messaging Based Monitoring (MBM) is to monitor the host it
runs on and to send what it finds to a messaging broker. Other components
receive and process these messages. This "mbm" package contains only the
skeleton; the "mbm-*" sub-packages implement the different components.

%package amqmonitor
Summary:	Messaging Based Monitoring (amqmonitor)
Group:		System
Requires:	mbm

%description amqmonitor
This MBM component monitors ActiveMQ brokers and generates corresponding "metric"
monitoring events.

%package carbon
Summary:	Messaging Based Monitoring (carbon)
Group:		System
Requires:	mbm
Requires:	stompclt

%description carbon
This MBM component receives "metric" monitoring messages, decodes them and
sends them to a Carbon daemon (using the plaintext protocol).

%package dnschecker
Summary:	Messaging Based Monitoring (dnschecker)
Group:		System
Requires:	mbm

%description dnschecker
This MBM component probes message brokers using DNS and generates
corresponding "check" monitoring events.

%package elasticsearch
Summary:	Messaging Based Monitoring (elasticsearch)
Group:		System
Requires:	mbm

%description elasticsearch
This MBM component receives "check", "log" and "status" monitoring messages,
decodes them and stores them into ElasticSearch.

%package forwarder
Summary:	Messaging Based Monitoring (forwarder)
Group:		System
Requires:	mbm

%description forwarder
This MBM component forwards "status" monitoring events outside of MBM. So far,
only mail forwarding is supported but others (e.g. SMS) will be added later.

%package httpchecker
Summary:	Messaging Based Monitoring (httpchecker)
Group:		System
Requires:	mbm

%description httpchecker
This MBM component probes web servers using HTTP and generates
corresponding "check" and "metric" monitoring events.

%package influxdb
Summary:	Messaging Based Monitoring (influxdb)
Group:		System
Requires:	mbm
Requires:	stompclt

%description influxdb
This MBM component receives "metric" monitoring messages, decodes them and
sends them to an InfluxDB server (using the line protocol).

%package logparser
Summary:	Messaging Based Monitoring (logparser)
Group:		System
Requires:	mbm

%description logparser
This MBM component parses log files and generates "log" monitoring events
everytime files change.

%package metis
Summary:	Messaging Based Monitoring (metis)
Group:		System
Requires:	mbm, mbm-sysmonitor

%description metis
This MBM component monitors Metis and generates corresponding "metric"
monitoring events.

%package monit
Summary:	Messaging Based Monitoring (monit)
Group:		System
Requires:	mbm

%description monit
This MBM component receives "check" monitoring events, decodes them and
sends them to MONIT (using HTTP POST).

%package sender
Summary:	Messaging Based Monitoring (sender)
Group:		System
Requires:	mbm
Requires:	stompclt

%description sender
This MBM component aggregates monitoring events into messages and sends them
to a messaging broker.

%package stompchecker
Summary:	Messaging Based Monitoring (stompchecker)
Group:		System
Requires:	mbm

%description stompchecker
This MBM component probes message brokers using STOMP and generates
corresponding "check" and "metric" monitoring events.

%package sysmonitor
Summary:	Messaging Based Monitoring (sysmonitor)
Group:		System
Requires:	mbm

%description sysmonitor
This MBM component monitors the operating system and generates corresponding
"metric" monitoring events.

%prep
%setup -q -n %{name}-%{version}

%build

%install
rm -fr %{buildroot}
# directories
install -m 0700 -d %{buildroot}%{home_dir}
install -m 0755 -d %{buildroot}%{bin_dir}
install -m 0755 -d %{buildroot}%{cfg_dir}
install -m 0755 -d %{buildroot}%{lib_dir}/Config/Generator/mbm
install -m 0755 -d %{buildroot}%{tpl_dir}
install -m 0700 -d %{buildroot}%{spool_dir}
install -m 0700 -d %{buildroot}%{run_dir}
install -m 0755 -d %{buildroot}%{perl_vendorlib}/MBM
# .d directories
for name in amqmon dnschk httpchk logparse mbmcg msg2out simplevisor stompchk sysmon; do
  install -m 0755 -d %{buildroot}%{home_dir}/etc/$name.d
done
# files
for name in bin/*; do
  install -m 0755 $name %{buildroot}%{bin_dir}/
done
for name in etc/*.conf; do
  install -m 0644 $name %{buildroot}%{cfg_dir}/
done
for name in etc/logparse.d/*.conf; do
  install -m 0644 $name %{buildroot}%{cfg_dir}/logparse.d/
done
for name in etc/msg2out.d/*; do
  install -m 0644 $name %{buildroot}%{cfg_dir}/msg2out.d/
done
for name in etc/simplevisor.d/*.svc; do
  install -m 0644 $name %{buildroot}%{cfg_dir}/simplevisor.d/
done
for name in etc/sysmon.d/*.conf; do
  install -m 0644 $name %{buildroot}%{cfg_dir}/sysmon.d/
done
for name in lib/Config/Generator/*.pm; do
  install -m 0644 $name %{buildroot}%{lib_dir}/Config/Generator/
done
for name in lib/Config/Generator/mbm/*.pm; do
  install -m 0644 $name %{buildroot}%{lib_dir}/Config/Generator/mbm/
done
install -m 0644 lib/MBM.pm %{buildroot}%{perl_vendorlib}/
for name in lib/MBM/*.pm; do
  install -m 0644 $name %{buildroot}%{perl_vendorlib}/MBM/
done
for name in tpl/*.tpl; do
  install -m 0644 $name %{buildroot}%{tpl_dir}/
done
# init.d/systemd
%if %{with_systemd}
install -m 0755 -d %{buildroot}%{_unitdir}/mbm.target.wants
install -m 0644 pkg/mbm.service %{buildroot}%{_unitdir}/mbm.service
install -m 0644 pkg/mbm.target  %{buildroot}%{_unitdir}/mbm.target
%else
install -m 0755 -d %{buildroot}/etc/init.d
ln -s %{bin_dir}/service %{buildroot}/etc/init.d/mbm
%endif

%clean
rm -fr %{buildroot}

%post
%if %{with_systemd}
%systemd_post mbm.service
%else
chkconfig --add mbm
%endif

%preun
%if %{with_systemd}
%systemd_preun mbm.service
if [ $1 -eq 0 ]; then
    rm -f %{_unitdir}/mbm.target.wants/*
fi
%else
if [ $1 -eq 0 ]; then
    service mbm stop
    chkconfig --del mbm
fi
%endif
if [ $1 -eq 0 ]; then
    rm -fr %{cfg_dir}/* %{spool_dir}/* %{run_dir}/*
fi

%preun amqmonitor
if [ $1 -eq 0 ]; then
    rm -f %{cfg_dir}/amqmon.d/*
fi

%preun dnschecker
if [ $1 -eq 0 ]; then
    rm -f %{cfg_dir}/dnschk.d/*
fi

%preun httpchecker
if [ $1 -eq 0 ]; then
    rm -f %{cfg_dir}/httpchk.d/*
fi

%preun logparser
if [ $1 -eq 0 ]; then
    rm -f %{cfg_dir}/logparse.d/*
fi

%preun stompchecker
if [ $1 -eq 0 ]; then
    rm -f %{cfg_dir}/stompchk.d/*
fi

%preun sysmonitor
if [ $1 -eq 0 ]; then
    rm -f %{cfg_dir}/sysmon.d/*
fi

%files
%defattr(-,root,root,-)
%doc README CHANGES
%dir %{home_dir}
%dir %{bin_dir}
%{bin_dir}/mbmcg
%{bin_dir}/service
%dir %{cfg_dir}
%dir %{cfg_dir}/simplevisor.d
%{cfg_dir}/mbmcg.conf
%{cfg_dir}/mbmcg.d
%dir %{lib_dir}
%dir %{lib_dir}/Config
%dir %{lib_dir}/Config/Generator
%dir %{lib_dir}/Config/Generator/mbm
%{lib_dir}/Config/Generator/mbm.pm
%dir %{tpl_dir}
%{tpl_dir}/receiver.conf.tpl
%{tpl_dir}/receiver.svc.tpl
%{tpl_dir}/simplevisor.conf.tpl
%{tpl_dir}/supervisor.svc.tpl
%{perl_vendorlib}/MBM.pm
%{perl_vendorlib}/MBM
%{spool_dir}
%{run_dir}
%if %{with_systemd}
%{_unitdir}/mbm.service
%{_unitdir}/mbm.target
%dir %{_unitdir}/mbm.target.wants
%else
/etc/init.d/mbm
%endif

%files amqmonitor
%defattr(-,root,root,-)
%{bin_dir}/amqmon
%{cfg_dir}/amqmon.d
%{cfg_dir}/simplevisor.d/amqmonitor.svc
%{lib_dir}/Config/Generator/mbm/amqmonitor.pm
%{tpl_dir}/amqmon*.tpl

%files carbon
%defattr(-,root,root,-)
%{bin_dir}/msg2car
%{lib_dir}/Config/Generator/mbm/carbon.pm
%{tpl_dir}/metric2carbon.*.tpl

%files dnschecker
%defattr(-,root,root,-)
%{bin_dir}/dnschk
%{cfg_dir}/dnschk.d
%{cfg_dir}/simplevisor.d/dnschecker.svc
%{lib_dir}/Config/Generator/mbm/dnschecker.pm
%{tpl_dir}/dnschk*.tpl

%files elasticsearch
%defattr(-,root,root,-)
%{bin_dir}/esbulk2es
%{bin_dir}/msg2esbulk
%{lib_dir}/Config/Generator/mbm/elasticsearch.pm
%{tpl_dir}/cls2esbulk.*.tpl
%{tpl_dir}/esbulk2es.*.tpl

%files forwarder
%defattr(-,root,root,-)
%{bin_dir}/msg2out
%{cfg_dir}/msg2out.d
%{lib_dir}/Config/Generator/mbm/forwarder.pm
%{tpl_dir}/msg2out.*.tpl

%files httpchecker
%defattr(-,root,root,-)
%{bin_dir}/httpchk
%{cfg_dir}/httpchk.d
%{cfg_dir}/simplevisor.d/httpchecker.svc
%{lib_dir}/Config/Generator/mbm/httpchecker.pm
%{tpl_dir}/httpchk*.tpl

%files influxdb
%defattr(-,root,root,-)
%{bin_dir}/msg2idb
%{lib_dir}/Config/Generator/mbm/influxdb.pm
%{tpl_dir}/metric2influxdb.*.tpl

%files logparser
%defattr(-,root,root,-)
%{bin_dir}/logparse
%{cfg_dir}/logparse.d
%{cfg_dir}/simplevisor.d/logparser.svc
%{lib_dir}/Config/Generator/mbm/logparser.pm
%{tpl_dir}/logparse*.tpl

%files metis
%defattr(-,root,root,-)
%{bin_dir}/metmon
%{cfg_dir}/simplevisor.d/metis.svc
%{lib_dir}/Config/Generator/mbm/metis.pm
%{tpl_dir}/metmon*.tpl

%files monit
%defattr(-,root,root,-)
%{bin_dir}/evt2monit

%files sender
%defattr(-,root,root,-)
%{bin_dir}/evt2msg
%{cfg_dir}/simplevisor.d/sender.svc
%{lib_dir}/Config/Generator/mbm/sender.pm
%{tpl_dir}/aggregator.*.tpl
%{tpl_dir}/sender.conf.tpl

%files stompchecker
%defattr(-,root,root,-)
%{bin_dir}/stompchk
%{cfg_dir}/stompchk.d
%{cfg_dir}/simplevisor.d/stompchecker.svc
%{lib_dir}/Config/Generator/mbm/stompchecker.pm
%{tpl_dir}/stompchk*.tpl

%files sysmonitor
%defattr(-,root,root,-)
%{bin_dir}/sysmon
%{cfg_dir}/sysmon.d
%{cfg_dir}/simplevisor.d/sysmonitor.svc
%{lib_dir}/Config/Generator/mbm/sysmonitor.pm
%{tpl_dir}/sysmon*.tpl
