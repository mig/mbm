#+##############################################################################
#                                                                              #
# File: MBM.pm                                                                 #
#                                                                              #
# Description: common code for messaging based monitoring                      #
#                                                                              #
#-##############################################################################

#
# module definition
#

package MBM;
use strict;
use warnings;

#
# used modules
#

use Authen::Credential qw();
use Config::General qw(ParseConfig);
use Config::Validator qw(*);
use Data::Dumper qw(Dumper);
use Getopt::Long qw(GetOptions);
use MBM::Event qw();
use MIME::Base64 qw(encode_base64);
use Net::Domain qw();
use No::Worries qw($ProgramName);
use No::Worries::Die qw(dief);
use No::Worries::Export qw(export_control);
use No::Worries::File qw(file_read);
use No::Worries::Log qw(log_debug log_filter);
use No::Worries::PidFile qw(*);
use No::Worries::Proc qw(proc_detach);
use No::Worries::Syslog qw(syslog_open syslog_close);
use No::Worries::Warn qw(warnf);
use Pod::Usage qw(pod2usage);
use Socket qw(inet_aton);
use Time::HiRes qw();

#
# global variables
#

our($Filter, $IdPre, $IdSeq, %NeedsCleanup, %Option, %Schema, $TimeLimit);

#
# return the (cleaned) host FQDN
#

sub host_fqdn () {
    my($fqdn);

    $fqdn = Net::Domain::hostfqdn();
    $fqdn =~ s/\.$//;
    return(lc($fqdn));
}

#
# generate a pseudo-unique id using URL-safe Base64 encoding (RFC3548)
#

sub unique_id () {
    my($tmp);

    unless ($IdPre) {
        $tmp = inet_aton(host_fqdn());
        dief("cannot find host IP address!") unless $tmp;
        $IdPre = encode_base64($tmp . pack("n", $$), "");
        $IdPre =~ tr[+/][-_];
        $IdSeq = 0;
    }
    $tmp = encode_base64(pack("Nn", time(), $IdSeq++), "");
    $tmp =~ tr[+/][-_];
    $IdSeq = 0 if $IdSeq == 65536;
    return($IdPre . $tmp);
}

#
# timer related helpers
#

sub timer_idle () {
    my($sleep);

    $sleep = $TimeLimit - Time::HiRes::time();
    return(0) if $sleep <= 0;
    $sleep = 1 if $sleep > 1;
    Time::HiRes::sleep($sleep);
    return(1);
}

sub timer_reset () {
    $TimeLimit = Time::HiRes::time() + $Option{interval};
}

#
# treeify the normal options (not the ones only for the configuration file)
#

sub treeify_options () {
    my(%tmp);

    # remove the options which are only for the configuration file
    foreach my $name (keys(%{ $Schema{config}{fields} })) {
        if ($name =~ /^_(.+)$/ and defined($Option{$1})) {
            $tmp{$1} = delete($Option{$1});
        }
    }
    # treeify the remaining options
    treeify(\%Option);
    # put back the removed options
    %Option = (%Option, %tmp);
}

#
# clean an option so that it complies to the schema
#

sub clean_option ($$$$@) {
    my($valid, $schema, $type, $data, @path) = @_;
    my($auth);

    if ($type eq "boolean") {
        return(0) if not defined($data) or is_true($data) or is_false($data);
        if ($data eq "0") {
            $_[3] = "false";
        } elsif ($data eq "1") {
            $_[3] = "true";
        }
        return(0);
    }
    if ($type eq "isa(Authen::Credential)") {
        if (ref($data) eq "HASH") {
            $auth = Authen::Credential->new($data);
        } elsif (ref($data) eq "") {
            $auth = Authen::Credential->parse($data);
        } else {
            dief("unexpected Authen::Credential: %s", $data);
        }
        $auth->check();
        $_[3] = $auth;
        return(0);
    }
    if ($type eq "valid(hash)") {
        if (ref($data) eq "HASH") {
            # as hash... nothing to do!
        } elsif (ref($data) eq "") {
            $_[3] = string2hash($data);
        } else {
            dief("unexpected hash: %s", $data);
        }
        return(0);
    }
    return(1)
}

#
# extend the schema with standard options
#

sub extend_schema () {
    $Schema{config}{fields}{batch} ||= {
        type => "integer",
        optional => "false",
    };
    $Schema{config}{fields}{daemon} ||= {
        type => "boolean",
        optional => "true",
    };
    $Schema{config}{fields}{debug} ||= {
        type => "integer",
        optional => "false",
    };
    $Schema{config}{fields}{filter} ||= {
        type => "string",
        optional => "true",
    };
    $Schema{config}{fields}{linger} ||= {
        type => "integer",
        optional => "false",
    };
    $Schema{config}{fields}{loop} ||= {
        type => "boolean",
        optional => "true",
    };
    $Schema{config}{fields}{pidfile} ||= {
        type => "string",
        optional => "true",
    };
    $Schema{config}{fields}{sleep} ||= {
        type => "number",
        optional => "false",
    };
}

#
# extract the list of command line options from the schema (with hacks!)
#

sub schema_options () {
    my(%fields, $validator, @options);

    # extend the schema with standard options
    extend_schema();
    # remove the options which are only for the configuration file
    foreach my $name (keys(%{ $Schema{config}{fields} })) {
        $fields{$name} = delete($Schema{config}{fields}{$name})
            if $name =~ /^_/;
    }
    # get the list of options from the hacked schema
    $validator = Config::Validator->new(%Schema);
    @options = (
        $validator->options("config"),
        # options below are only for command line
        "config|conf=s",
        "help|h|?",
        "list|l",
        "manual|m",
        "quit",
        "status",
    );
    # put back the options which are only for the configuration file
    $Schema{config}{fields} = { %{ $Schema{config}{fields} }, %fields };
    # hack some option definitions
    foreach my $option (@options) {
        # --debug can be repeated
        $option =~ s/^(debug)(.+)$/$1|d+/;
    }
    return(@options);
}

#
# (parse the command line options and) handle the --config option
#

sub handle_config (@) {
    my(@options) = @_;
    my(@list, %tmp);

    # first pass
    @list = @ARGV;
    Getopt::Long::Configure(qw(posix_default no_ignore_case));
    GetOptions(\%tmp, @options) or pod2usage(2);
    pod2usage(1) if $tmp{help};
    pod2usage(exitstatus => 0, verbose => 2) if $tmp{manual};
    pod2usage(2) if @ARGV;
    if ($tmp{list}) {
        print("Here are the supported command line options:\n");
        foreach my $option (sort(@options)) {
            printf("  --%s\n", $option);
        }
        exit(0);
    }
    if (defined($tmp{config})) {
        # a second pass will be needed after having read the configuration
        %Option = ParseConfig(
            -CComments       => 0,
            -ConfigFile      => $tmp{config},
            -IncludeRelative => 1,
            -IncludeGlob     => 1,
        );
        # treeify the normal options
        treeify_options();
        # second pass
        @ARGV = @list;
        GetOptions(\%Option, @options) or pod2usage(2);
        delete($Option{config});
    } else {
        # the first pass is enough
        %Option = %tmp;
    }
}

#
# handle the --daemon option
#

sub handle_daemon () {
    $No::Worries::Log::Handler = \&No::Worries::Syslog::log2syslog;
    $No::Worries::Die::Syslog = 1;
    $No::Worries::Warn::Syslog = 1;
    syslog_open(ident => $ProgramName, facility => "user");
    $NeedsCleanup{syslog}++;
    proc_detach(callback => sub {
        printf("%s (pid %d) started\n", $ProgramName, $_[0])
    });
    log_debug("daemonized");
}

#
# handle the --filter option
#

sub handle_filter () {
    if ($Option{filter} =~ /\n/) {
        $Filter = MBM::Event::filter($Option{filter});
        log_debug("using inline filter");
    } else {
        $Filter = MBM::Event::filter(file_read($Option{filter}));
        log_debug("using filter from %s", $Option{filter});
    }
}

#
# handle the --pidfile option
#

sub handle_pidfile () {
    pf_set($Option{pidfile});
    $NeedsCleanup{pidfile}++;
}

#
# handle the --quit option
#

sub handle_quit () {
    dief("missing mandatory option for --quit: --pidfile")
        unless $Option{pidfile};
    pf_quit($Option{pidfile},
        linger => $Option{linger},
        callback => sub { printf("%s %s\n", $ProgramName, $_[0]) },
    );
    exit(0);
}

#
# handle the --status option
#

sub handle_status () {
    my($status, $message, $code);

    dief("missing mandatory option for --status: --pidfile")
        unless $Option{pidfile};
    ($status, $message, $code) =
        pf_status($Option{pidfile}, freshness => $Option{linger});
    printf("%s %s\n", $ProgramName, $message);
    exit($code);
}

#
# parse the command line options and handle the most standard ones
#

sub handle_options (%) {
    my(%default) = @_;
    my(%fields, $validator);

    # parse the command line options
    handle_config(schema_options());
    # handle the defaults
    $default{batch} = 100 unless defined($default{batch});
    $default{debug} = 0 unless defined($default{debug});
    $default{linger} = 10 unless defined($default{linger});
    $default{sleep} = 1 unless defined($default{sleep});
    foreach my $name (keys(%default)) {
        $Option{$name} = $default{$name} unless defined($Option{$name});
    }
    # handle basic options
    log_filter("debug") if $Option{debug};
    handle_quit() if $Option{quit};
    handle_status() if $Option{status};
    handle_filter() if $Option{filter};
    # treeify the normal options
    treeify_options();
    # handle the options which are only for the configuration file
    foreach my $name (keys(%{ $Schema{config}{fields} })) {
        if ($name =~ /^_(.+)$/) {
            $fields{$1} = delete($Schema{config}{fields}{$name});
        }
    }
    $Schema{config}{fields} = { %{ $Schema{config}{fields} }, %fields };
    # cleanup and validation
    $validator = Config::Validator->new(%Schema);
    $validator->traverse(\&clean_option, \%Option, "config");
    if ($Option{debug} > 1) {
        $Data::Dumper::Sortkeys = 1;
        printf(STDERR "# %s configuration: %s", $ProgramName, Dumper(\%Option));
    }
    $validator->validate(\%Option, "config");
    # make sure the toplevel boolean options can be tested directly
    foreach my $name (keys(%{ $Schema{config}{fields} })) {
        next unless $Schema{config}{fields}{$name}{type} eq "boolean";
        delete($Option{$name}) unless is_true($Option{$name});
    }
    # timer initialization
    $TimeLimit = 0;
}

#
# handle looping over a given task
#

sub handle_looping ($) {
    my($task) = @_;
    my($signal, $busy, $end, $sleep);

    # loop until ^C or --quit
    local $SIG{INT}  = sub { $signal = "SIGINT" };
    local $SIG{QUIT} = sub { $signal = "SIGQUIT" };
    local $SIG{TERM} = sub { $signal = "SIGTERM" };
    $signal = "";
    while (not $signal) {
        $busy = $task->();
        $end = $busy ? 0 : Time::HiRes::time() + $Option{sleep};
        while (not $signal) {
            if ($Option{pidfile}) {
                if (pf_check($Option{pidfile}) eq "quit") {
                    log_debug("told to quit");
                    return;
                }
                pf_touch($Option{pidfile});
            }
            $sleep = $end - Time::HiRes::time();
            last if $sleep <= 0;
            $sleep = 1 if $sleep > 1;
            Time::HiRes::sleep($sleep);
        }
    }
    log_debug("caught $signal") if $signal;
}

#
# main code
#

sub main ($) {
    my($task) = @_;

    if ($Option{loop}) {
        handle_looping($task);
    } else {
        $task->();
    }
}

#
# cleanup
#

END {
    return if $No::Worries::Proc::Transient;
    log_debug("cleanup");
    pf_unset($Option{pidfile}) if $NeedsCleanup{pidfile};
    syslog_close() if $NeedsCleanup{syslog};
}

#
# export control
#

sub import : method {
    my($pkg, %exported);

    $pkg = shift(@_);
    grep($exported{$_}++, qw(%Option %Schema $Filter));
    export_control(scalar(caller()), $pkg, \%exported, @_);
}

1;
