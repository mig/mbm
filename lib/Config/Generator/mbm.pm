#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm.pm                                                #
#                                                                              #
# Description: messaging based monitoring abstraction                          #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm;
use strict;
use warnings;

#
# used modules
#

use Authen::Credential qw();
use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
use Config::Generator::Util qw(format_profile);
use Config::Validator qw(is_true);
use Net::Domain qw();
use No::Worries::Die qw(dief);
use No::Worries::Dir qw(dir_read);
use Socket qw(AF_INET inet_ntoa);

#
# load all the sub-modules (skipping the one calling us, if any)
#

sub load_submodules () {
    my($caller, $module);

    $caller = caller(2) || "";
    foreach my $name (dir_read(substr(__FILE__, 0, -3))) {
        if ($name =~ /^(\w+)\.pm$/) {
            $module = "Config::Generator::mbm::$1";
            next if $module eq $caller;
            eval("require $module"); ## no critic 'ProhibitStringyEval'
            if ($@) {
                $@ =~ s/\s+at\s.+?\sline\s+\d+\.?$//;
                dief("failed to load %s: %s", $module, $@);
            }
        }
    }
}

#
# module initialization (including defining the schemas)
#

sub init () {
    # define the "/mbm/broker" schema (= broker to send to or receive from)
    register_schema("/mbm/broker", {
        type => "struct",
        fields => {
            host => REQ_HOSTNAME,
            port => REQ_INTEGER,
            ssl => DEF_BOOLEAN,
            auth => {
                type => [qw(undefined string table(string))],
                optional => "incfg",
            },
        },
    });
    # define the "/mbm/client" schema (= messaging client of a broker instance)
    register_schema("/mbm/client", {
        type => "struct",
        fields => {
            producer => OPT_STRING,
            consumer => OPT_STRING,
        },
    });
    # define the "/mbm/instance" schema (= locally running broker instance)
    register_schema("/mbm/instance", {
        type => "struct",
        fields => {
            confdir => DEF_PATH,
            logdir => DEF_PATH,
            type => REQ_STRING,
            port => {
                type => "table(string)",
                optional => "false",
            },
            client => {
                type => "table(valid(/mbm/client))",
                optional => "true",
            },
        },
    });
    # define the "/mbm/subscription" schema (= subscription to receive messages)
    register_schema("/mbm/subscription", {
        type => "string",
        optional => "false",
    });
    # define the "/mbm" schema
    register_schema("/mbm", {
        type => "struct",
        fields => {
            host => DEF_HOSTNAME,
            bindir => DEF_PATH,
            confdir => DEF_PATH,
            rundir => DEF_PATH,
            spooldir => DEF_PATH,
            destination => DEF_STRING,
            broker => {
                type => "valid(/mbm/broker)",
                optional => "false",
            },
            instance => {
                type => "table(valid(/mbm/instance))",
                optional => "true",
            },
        },
    });
    # declare the "/mbm" subtree as mandatory
    mandatory_subtree("/mbm");
    # declare the templates we use
    declare_template(qw(simplevisor.conf));
    # declare the shared templates sub-modules can use
    declare_template(qw(receiver.svc receiver.conf));
    # load all the sub-modules
    load_submodules();
}

#
# configuration checking (including setting the defaults)
#

sub check_instance ($) {
    my($name) = @_;
    my($cfg);

    $cfg = $Config{mbm}{instance}{$name};
    $cfg->{confdir} ||= "/var/mb/$name/etc";
    $cfg->{logdir} ||= "/var/mb/$name/log";
    dief("unexpected instance type: %s", $cfg->{type})
        unless $cfg->{type} and $cfg->{type} =~ /^(activemq)$/;
    foreach my $port (keys(%{ $cfg->{port} })) {
        dief("unexpected port number: %s", $port)
            unless $port =~ /^\d+$/;
        dief("unexpected port name: %s", $cfg->{port}{$port})
            unless $cfg->{port}{$port} =~ /^[\w\-\+]+$/;
    }
    foreach my $client (keys(%{ $cfg->{client} })) {
        dief("unexpected client name: %s", $client)
            unless $client =~ /^[\w\-\+]+$/;
    }
}

sub check () {
    my($cfg, $tmp);

    $cfg = $Config{mbm};
    $cfg->{host} ||= Net::Domain::hostname();
    $cfg->{bindir} ||= "/opt/mbm/bin";
    $cfg->{confdir} ||= "/opt/mbm/etc";
    $cfg->{rundir} ||= "/var/run/mbm";
    $cfg->{spooldir} ||= "/var/spool/mbm";
    $cfg->{destination} ||= "event.{type}";
    $cfg = $Config{mbm}{broker} ||= {};
    $cfg->{ssl} ||= "false";
    # set a default authentication
    unless ($cfg->{auth}) {
        if (is_true($cfg->{ssl})) {
            $tmp = "/etc/grid-security";
            $cfg->{auth} = {
                scheme => "x509",
                ca     => "${tmp}/certificates",
                key    => "${tmp}/hostkey.pem",
                cert   => "${tmp}/hostcert.pem",
            };
        } else {
            $cfg->{auth} = "none";
        }
    }
    # validate carefully the authentication
    if (ref($cfg->{auth}) eq "HASH") {
        $tmp = Authen::Credential->new($cfg->{auth});
    } elsif (ref($cfg->{auth}) eq "") {
        $tmp = Authen::Credential->parse($cfg->{auth});
    } else {
        dief("unexpected Authen::Credential: %s", $cfg->{auth});
    }
    $tmp->check();
    $cfg->{auth} = { $tmp->hash() };
    # check the broker instances
    foreach my $name (keys(%{ $Config{mbm}{instance} })) {
        check_instance($name);
    }
}

#
# return a hash with all the brokers that can be used
#

sub brokers () {
    my(%broker, @addrs, $host, $name);

    $host = $Config{mbm}{broker}{host};
    @addrs = gethostbyname($host);
    dief("unknown host: %s", $host) unless @addrs;
    splice(@addrs, 0, 4);
    foreach my $addr (@addrs) {
        $host = gethostbyaddr($addr, AF_INET);
        dief("unknown address: %s", inet_ntoa($addr)) unless $host;
        ($name = $host) =~ s/\..+//;
        $broker{$name} = $host;
    }
    return(%broker);
}

#
# return a hash with the configuration of a given broker
#

sub broker_configuration (;$) {
    my($host) = @_;
    my($cfg, %config);

    $cfg = $Config{mbm}{broker};
    $host ||= $cfg->{host};
    $config{uri} = sprintf("%s://%s:%d",
        is_true($cfg->{ssl}) ? "stomp+ssl" : "stomp", $host, $cfg->{port});
    $config{auth} = Authen::Credential->new($cfg->{auth})->string();
    $config{sockopts} = is_true($cfg->{ssl}) ? "SSL_verify_mode=1" : "";
    return(%config);
}

#
# return a string with the subscription configuration
#

sub subscription_configuration (@) {
    my(@subscriptions) = @_;
    my($destination, @list);

    foreach my $subscription (@subscriptions) {
        $destination = $subscription;
        $destination =~ s/\{host\}/$Config{mbm}{host}/g;
        push(@list, $destination);
    }
    @list = map("<subscribe>\n  destination = \"$_\"\n</subscribe>", @list);
    return(join("\n\n", @list));
}

#
# profile generation
#

sub generate_profile () {
    my($path, $contents);

    $path = "$Config{mbm}{confdir}/profile";
    $contents = format_profile(
        "#" => "",
        "#" => "mbm profile",
        "#" => "",
        MBM_BINDIR   => $Config{mbm}{bindir},
        MBM_CONFDIR  => $Config{mbm}{confdir},
        MBM_RUNDIR   => $Config{mbm}{rundir},
        MBM_SPOOLDIR => $Config{mbm}{spooldir},
    );
    ensure_file($path, $contents);
}

#
# simplevisor generation
#

sub generate_simplevisor () {
    my($path);

    $path = "$Config{mbm}{confdir}/simplevisor.conf";
    ensure_file($path, expand_template("simplevisor.conf"));
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_profile);
register_hook("generate", \&generate_simplevisor);

1;
