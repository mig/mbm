#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/httpchecker.pm                                    #
#                                                                              #
# Description: messaging based monitoring httpchecker abstraction              #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::httpchecker;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
use No::Worries::Die qw(dief);
# dependencies
use Config::Generator::mbm qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/mbm" schema to add an optional "httpchecker" field
    extend_schema("/mbm", {
        httpchecker => {
            type => "struct",
            fields => {
                threads => DEF_INTEGER,
            },
            optional => "incfg",
        },
    });
    # declare the templates we use
    declare_template(qw(httpchk.conf));
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $cfg = $Config{mbm}{httpchecker} ||= {};
    $cfg->{threads} ||= 10;
}

#
# configuration generation
#

sub generate_configuration () {
    my($path);

    $path = "$Config{mbm}{confdir}/httpchk.conf";
    ensure_file($path, expand_template("httpchk.conf"));
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_configuration);

1;
