#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/amqmonitor.pm                                     #
#                                                                              #
# Description: messaging based monitoring amqmonitor abstraction               #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::amqmonitor;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Template qw(*);
use No::Worries::Die qw(dief);
# dependencies
use Config::Generator::mbm qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # declare the templates we use
    declare_template(qw(amqmon.conf));
}

#
# generate a per-broker configuration
#

sub generate_broker_configuration ($) {
    my($name) = @_;
    my($cfg, $contents, $jetty);

    $cfg = $Config{mbm}{instance}{$name};
    foreach my $port (keys(%{ $cfg->{port} })) {
        $jetty = $port if $cfg->{port}{$port} eq "jetty";
    }
    dief("unexpected ActiveMQ broker configuration (no jetty port): %s", $name)
        unless $jetty;
    $contents = "";
    $contents .= "#\n";
    $contents .= "# amqmon configuration ($name broker)\n";
    $contents .= "#\n";
    $contents .= "\n";
    $contents .= "<$name>\n";
    $contents .= "  port = $jetty\n";
    $contents .= "  <client>\n";
    foreach my $client (sort(keys(%{ $cfg->{client} }))) {
        $contents .= "    <$client>\n";
        foreach my $key (qw(producer consumer)) {
            next unless $cfg->{client}{$client}{$key};
            $contents .= "      $key = $cfg->{client}{$client}{$key}\n";
        }
        $contents .= "    </$client>\n";
    }
    $contents .= "  </client>\n";
    $contents .= "</$name>\n";
    return($contents);
}

#
# configuration generation
#

sub generate_configuration () {
    my($path);

    $path = "$Config{mbm}{confdir}/amqmon.conf";
    ensure_file($path, expand_template("amqmon.conf"));
    foreach my $name (keys(%{ $Config{mbm}{instance} })) {
        next unless $Config{mbm}{instance}{$name}{type} eq "activemq";
        $path = "$Config{mbm}{confdir}/amqmon.d/$name.conf";
        ensure_file($path, generate_broker_configuration($name));
    }
}

#
# registration
#

init();
register_hook("generate", \&generate_configuration);

1;
