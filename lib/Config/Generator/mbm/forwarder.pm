#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/forwarder.pm                                      #
#                                                                              #
# Description: messaging based monitoring forwarder abstraction                #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::forwarder;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
use Config::Validator qw(listof);
# dependencies
use Config::Generator::mbm qw();

#
# constants
#

our $ID  = "forwarder";
our $ID1 = "msg2out";

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/mbm" schema to add an optional "forwarder" field
    extend_schema("/mbm", {
        forwarder => {
            type => "struct",
            fields => {
                subscription => {
                    type => "list?(valid(/mbm/subscription))",
                    optional => "incfg",
                },
            },
            optional => "incfg",
        },
    });
    # declare the templates we use
    declare_template("sysmon.$ID.conf", "$ID1.conf", "$ID1.svc");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $cfg = $Config{mbm}{forwarder} ||= {};
    unless ($cfg->{subscription}) {
        $cfg->{subscription} = "/queue/Consumer.{host}.metis.evt.status";
    }
}

#
# configuration generation
#

sub generate_configuration () {
    my($path, %broker, $subscription, %config);

    # supervisor configuration
    $path = "$Config{mbm}{confdir}/simplevisor.d/$ID.svc";
    ensure_file($path, expand_template("supervisor.svc", id => $ID));
    # receiver(s) configuration
    %broker = Config::Generator::mbm::brokers();
    $subscription = Config::Generator::mbm::subscription_configuration(
        listof($Config{mbm}{forwarder}{subscription}),
    );
    foreach my $name (keys(%broker)) {
        %config = (id => $ID, broker => $name);
        $path = "$Config{mbm}{confdir}/simplevisor.d/$ID/$name.svc";
        ensure_file($path, expand_template("receiver.svc", %config));
        $path = "$Config{mbm}{confdir}/$ID-$name.conf";
        %config = Config::Generator::mbm::broker_configuration($broker{$name});
        $config{id} = $ID;
        $config{broker} = $name;
        $config{subscribe} = $subscription;
        ensure_file($path, expand_template("receiver.conf", %config));
    }
    # msg2out configuration
    $path = "$Config{mbm}{confdir}/$ID1.conf";
    ensure_file($path, expand_template("$ID1.conf", id => $ID));
    $path = "$Config{mbm}{confdir}/simplevisor.d/$ID/$ID1.svc";
    ensure_file($path, expand_template("$ID1.svc"));
    # sysmon configuration
    $path = "$Config{mbm}{confdir}/sysmon.d/$ID.conf";
    ensure_file($path, expand_template("sysmon.$ID.conf"));
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_configuration);

1;
