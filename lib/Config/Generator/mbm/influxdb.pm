#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/influxdb.pm                                       #
#                                                                              #
# Description: messaging based monitoring InfluxDB abstraction                 #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::influxdb;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
use Config::Validator qw(listof);
# dependencies
use Config::Generator::mbm qw();

#
# constants
#

our $ID  = "influxdb";
our $ID1 = "metric2influxdb";

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/mbm" schema to add an optional "influxdb" field
    extend_schema("/mbm", {
        influxdb => {
            type => "struct",
            fields => {
                auth => DEF_STRING,
                subscription => {
                    type => "list?(valid(/mbm/subscription))",
                    optional => "incfg",
                },
                url => DEF_STRING,
            },
            optional => "incfg",
        },
    });
    # declare the templates we use
    declare_template("sysmon.$ID.conf", "$ID1.conf", "$ID1.svc");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg, $destination);

    $cfg = $Config{mbm}{influxdb} ||= {};
    $cfg->{auth} ||= "none";
    $cfg->{url} ||= "http://localhost:8086/write?db=mbm";
    unless ($cfg->{subscription}) {
        $destination = "/queue/Consumer.{host}.$Config{mbm}{destination}";
        $destination =~ s/\{type\}/metric/;
        $cfg->{subscription} = $destination;
    }
}

#
# configuration generation
#

sub generate_configuration () {
    my($path, %broker, $subscription, %config);

    # supervisor configuration
    $path = "$Config{mbm}{confdir}/simplevisor.d/$ID.svc";
    ensure_file($path, expand_template("supervisor.svc", id => $ID));
    # receiver(s) configuration
    %broker = Config::Generator::mbm::brokers();
    $subscription = Config::Generator::mbm::subscription_configuration(
        listof($Config{mbm}{influxdb}{subscription}),
    );
    foreach my $name (keys(%broker)) {
        %config = (id => $ID, broker => $name);
        $path = "$Config{mbm}{confdir}/simplevisor.d/$ID/$name.svc";
        ensure_file($path, expand_template("receiver.svc", %config));
        $path = "$Config{mbm}{confdir}/$ID-$name.conf";
        %config = Config::Generator::mbm::broker_configuration($broker{$name});
        $config{id} = $ID;
        $config{broker} = $name;
        $config{subscribe} = $subscription;
        ensure_file($path, expand_template("receiver.conf", %config));
    }
    # metric2influxdb configuration
    $path = "$Config{mbm}{confdir}/$ID1.conf";
    ensure_file($path, expand_template("$ID1.conf", id => $ID));
    $path = "$Config{mbm}{confdir}/simplevisor.d/$ID/$ID1.svc";
    ensure_file($path, expand_template("$ID1.svc"));
    # sysmon configuration
    $path = "$Config{mbm}{confdir}/sysmon.d/$ID.conf";
    ensure_file($path, expand_template("sysmon.$ID.conf"));
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_configuration);

1;
