#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/sysmonitor.pm                                     #
#                                                                              #
# Description: messaging based monitoring sysmonitor abstraction               #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::sysmonitor;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Template qw(*);
use No::Worries::Die qw(dief);
# dependencies
use Config::Generator::mbm qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # declare the templates we use
    declare_template(qw(sysmon.conf sysmon.mbm.conf));
}

#
# generate a per-broker configuration
#

sub generate_broker_configuration ($) {
    my($name) = @_;
    my($cfg, $contents, @list);

    $cfg = $Config{mbm}{instance}{$name};
    $contents = "";
    $contents .= "#\n";
    $contents .= "# sysmon configuration ($name broker)\n";
    $contents .= "#\n";
    # process
    if ($cfg->{type} eq "activemq") {
        @list = map(quotemeta($_),
                    "org.apache.activemq.console.Main",
                    "start",
                    "xbean:file:$cfg->{confdir}/activemq.xml");
    } else {
        dief("unsupported broker type: %s", $cfg->{type});
    }
    $contents .= "\n";
    $contents .= "<process>\n";
    $contents .= "  _match = \" @list \"\n";
    $contents .= "  broker = $name\n";
    $contents .= "  group = msgbrk\n";
    $contents .= "</process>\n";
    # disk
    $contents .= "\n";
    $contents .= "<disk>\n";
    $contents .= "  _paths = /var/mb*/$name\n";
    $contents .= "  broker = $name\n";
    $contents .= "  group = msgbrk\n";
    $contents .= "</disk>\n";
    # network
    @list = sort({ $a <=> $b } keys(%{ $cfg->{port} }));
    foreach my $port (@list) {
        $contents .= "\n";
        $contents .= "<network>\n";
        $contents .= "  proto = tcp\n";
        $contents .= "  port = $port\n";
        $contents .= "  broker = $name\n";
        $contents .= "  group = msgsvc\n";
        $contents .= "</network>\n";
    }
    return($contents);
}

#
# configuration generation
#

sub generate_configuration () {
    my($path);

    $path = "$Config{mbm}{confdir}/sysmon.conf";
    ensure_file($path, expand_template("sysmon.conf"));
    $path = "$Config{mbm}{confdir}/sysmon.d/mbm.conf";
    ensure_file($path, expand_template("sysmon.mbm.conf"));
    foreach my $name (keys(%{ $Config{mbm}{instance} })) {
        $path = "$Config{mbm}{confdir}/sysmon.d/mb-$name.conf";
        ensure_file($path, generate_broker_configuration($name));
    }
}

#
# registration
#

init();
register_hook("generate", \&generate_configuration);

1;
