#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/dnschecker.pm                                     #
#                                                                              #
# Description: messaging based monitoring dnschecker abstraction               #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::dnschecker;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Template qw(*);
use No::Worries::Die qw(dief);
# dependencies
use Config::Generator::mbm qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # declare the templates we use
    declare_template(qw(dnschk.conf));
}

#
# configuration generation
#

sub generate_configuration () {
    my($path);

    $path = "$Config{mbm}{confdir}/dnschk.conf";
    ensure_file($path, expand_template("dnschk.conf"));
}

#
# registration
#

init();
register_hook("generate", \&generate_configuration);

1;
