#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/elasticsearch.pm                                  #
#                                                                              #
# Description: messaging based monitoring ElasticSearch abstraction            #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::elasticsearch;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
use Config::Validator qw(listof);
use No::Worries::Die qw(dief);
# dependencies
use Config::Generator::mbm qw();

#
# constants
#

our $ID  = "elasticsearch";
our $ID1 = "cls2esbulk";
our $ID2 = "esbulk2es";

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/mbm" schema to add an optional "elasticsearch" field
    extend_schema("/mbm", {
        elasticsearch => {
            type => "struct",
            fields => {
                auth => DEF_STRING,
                count => DEF_INTEGER,
                subscription => {
                    type => "list?(valid(/mbm/subscription))",
                    optional => "incfg",
                },
                url => REQ_STRING,
            },
            optional => "incfg",
        },
    });
    # declare the templates we use
    declare_template("sysmon.$ID.conf", "$ID1.conf", "$ID1.svc",
                     "$ID2.conf", "$ID2.svc");
}

#
# configuration checking (including setting the defaults)
#

sub check1 () {
    my($cfg, $destination);

    $cfg = $Config{mbm}{elasticsearch} ||= {};
    $cfg->{auth} ||= "none";
    $cfg->{count} ||= 1;
    dief("invalid elasticsearch inserter count: %d", $cfg->{count})
        unless 1 <= $cfg->{count} and $cfg->{count} <= 9;
    unless ($cfg->{subscription}) {
        $cfg->{subscription} = [];
        foreach my $type (qw(check log status)) {
            $destination = "/queue/Consumer.{host}.$Config{mbm}{destination}";
            $destination =~ s/\{type\}/$type/;
            push(@{ $cfg->{subscription} }, $destination);
        }
    }
}

#
# configuration generation
#

sub generate_configuration () {
    my($path, %broker, $subscription, %config, @ids);

    # supervisor configuration
    $path = "$Config{mbm}{confdir}/simplevisor.d/$ID.svc";
    ensure_file($path, expand_template("supervisor.svc", id => $ID));
    # receiver(s) configuration
    %broker = Config::Generator::mbm::brokers();
    $subscription = Config::Generator::mbm::subscription_configuration(
        listof($Config{mbm}{elasticsearch}{subscription}),
    );
    foreach my $name (keys(%broker)) {
        %config = (id => $ID, broker => $name);
        $path = "$Config{mbm}{confdir}/simplevisor.d/$ID/$name.svc";
        ensure_file($path, expand_template("receiver.svc", %config));
        $path = "$Config{mbm}{confdir}/$ID-$name.conf";
        %config = Config::Generator::mbm::broker_configuration($broker{$name});
        $config{id} = $ID;
        $config{broker} = $name;
        $config{subscribe} = $subscription;
        ensure_file($path, expand_template("receiver.conf", %config));
    }
    # cls2esbulk configuration
    $path = "$Config{mbm}{confdir}/$ID1.conf";
    ensure_file($path, expand_template("$ID1.conf", id => $ID));
    $path = "$Config{mbm}{confdir}/simplevisor.d/$ID/$ID1.svc";
    ensure_file($path, expand_template("$ID1.svc"));
    # esbulk2es configuration
    if ($Config{mbm}{elasticsearch}{count} == 1) {
        @ids = ("");
    } else {
        @ids = (1 .. $Config{mbm}{elasticsearch}{count});
    }
    $path = "$Config{mbm}{confdir}/$ID2.conf";
    ensure_file($path, expand_template("$ID2.conf"));
    foreach my $id (@ids) {
        $path = "$Config{mbm}{confdir}/simplevisor.d/$ID/$ID2$id.svc";
        ensure_file($path, expand_template("$ID2.svc", id => $id));
    }
    # sysmon configuration
    $path = "$Config{mbm}{confdir}/sysmon.d/$ID.conf";
    ensure_file($path, expand_template("sysmon.$ID.conf"));
}

#
# registration
#

init();
register_hook("check", \&check1);
register_hook("generate", \&generate_configuration);

1;
