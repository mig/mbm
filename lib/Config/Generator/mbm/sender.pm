#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/sender.pm                                         #
#                                                                              #
# Description: messaging based monitoring aggregator+sender abstraction        #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::sender;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Template qw(*);
# dependencies
use Config::Generator::mbm qw();

#
# module initialization
#

sub init () {
    # declare the templates we use
    declare_template(qw(aggregator.conf aggregator.svc sender.conf));
}

#
# aggregator configuration generation
#

sub generate_aggregator () {
    my($path);

    # daemon configuration
    $path = "$Config{mbm}{confdir}/aggregator.conf";
    ensure_file($path, expand_template("aggregator.conf"));
    # service configuration
    $path = "$Config{mbm}{confdir}/simplevisor.d/aggregator.svc";
    ensure_file($path, expand_template("aggregator.svc"));
}

#
# sender configuration generation
#

sub generate_sender () {
    my($path, %config);

    $path = "$Config{mbm}{confdir}/sender.conf";
    %config = Config::Generator::mbm::broker_configuration();
    ensure_file($path, expand_template("sender.conf", %config));
}

#
# registration
#

init();
register_hook("generate", \&generate_aggregator);
register_hook("generate", \&generate_sender);

1;
