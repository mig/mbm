#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/logparser.pm                                      #
#                                                                              #
# Description: messaging based monitoring logparser abstraction                #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::logparser;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
use No::Worries::Die qw(dief);
# dependencies
use Config::Generator::mbm qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/mbm" schema to add an optional "logparser" field
    extend_schema("/mbm", {
        logparser => {
            type => "struct",
            fields => {
                callback => OPT_NAME,
            },
            optional => "true",
        },
    });
    # declare the templates we use
    declare_template(qw(logparse.conf));
    declare_template(map("logparse.$_.conf", qw(activemq)));
}

#
# configuration generation
#

sub generate_configuration () {
    my($path, $type);

    $path = "$Config{mbm}{confdir}/logparse.conf";
    ensure_file($path, expand_template("logparse.conf"));
    foreach my $name (keys(%{ $Config{mbm}{instance} })) {
        $path = "$Config{mbm}{confdir}/logparse.d/$name.conf";
        $type = $Config{mbm}{instance}{$name}{type};
        ensure_file($path, expand_template("logparse.$type.conf",
            logdir => $Config{mbm}{instance}{$name}{logdir},
            name => $name,
        ));
    }
}

#
# registration
#

init();
register_hook("generate", \&generate_configuration);

1;
