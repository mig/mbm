#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/metis.pm                                          #
#                                                                              #
# Description: messaging based monitoring metis abstraction                    #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::metis;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
# dependencies
use Config::Generator::mbm qw();

#
# constants
#

our $ID = "metis";

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/mbm" schema to add an optional "metis" field
    extend_schema("/mbm", {
        metis => {
            type => "struct",
            fields => {
                base => REQ_PATH,
                logdir => DEF_PATH,
                port => DEF_INTEGER,
                spooldir => DEF_PATH,
            },
            optional => "incfg",
        },
    });
    # declare the templates we use
    declare_template(qw(metmon.conf), "sysmon.$ID.conf");
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $cfg = $Config{mbm}{metis};
    $cfg->{logdir} ||= "$cfg->{base}/log";
    $cfg->{port} ||= 1104;
    $cfg->{spooldir} ||= "$cfg->{base}/spool";
}

#
# configuration generation
#

sub generate_configuration () {
    my($path);

    # metmon configuration
    $path = "$Config{mbm}{confdir}/metmon.conf";
    ensure_file($path, expand_template("metmon.conf"));
    # sysmon configuration
    $path = "$Config{mbm}{confdir}/sysmon.d/$ID.conf";
    ensure_file($path, expand_template("sysmon.$ID.conf"));
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_configuration);

1;
