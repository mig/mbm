#+##############################################################################
#                                                                              #
# File: Config/Generator/mbm/stompchecker.pm                                   #
#                                                                              #
# Description: messaging based monitoring stompchecker abstraction             #
#                                                                              #
#-##############################################################################

#
# module definition
#

package Config::Generator::mbm::stompchecker;
use strict;
use warnings;

#
# used modules
#

use Config::Generator qw(%Config);
use Config::Generator::File qw(*);
use Config::Generator::Hook qw(register_hook);
use Config::Generator::Schema qw(*);
use Config::Generator::Template qw(*);
use No::Worries::Die qw(dief);
# dependencies
use Config::Generator::mbm qw();

#
# module initialization (including defining the schemas)
#

sub init () {
    # extend the "/mbm" schema to add an optional "stompchecker" field
    extend_schema("/mbm", {
        stompchecker => {
            type => "struct",
            fields => {
                threads => DEF_INTEGER,
            },
            optional => "incfg",
        },
    });
    # declare the templates we use
    declare_template(qw(stompchk.conf));
}

#
# configuration checking (including setting the defaults)
#

sub check () {
    my($cfg);

    $cfg = $Config{mbm}{stompchecker} ||= {};
    $cfg->{threads} ||= 10;
}

#
# configuration generation
#

sub generate_configuration () {
    my($path);

    $path = "$Config{mbm}{confdir}/stompchk.conf";
    ensure_file($path, expand_template("stompchk.conf"));
}

#
# registration
#

init();
register_hook("check", \&check);
register_hook("generate", \&generate_configuration);

1;
