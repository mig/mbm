#+##############################################################################
#                                                                              #
# File: MBM/Event.pm                                                           #
#                                                                              #
# Description: messaging based monitoring event module                         #
#                                                                              #
#-##############################################################################

#
# module definition
#

package MBM::Event;
use strict;
use warnings;

#
# used modules
#

use JSON qw();
use No::Worries::Die qw(dief);

#
# global variables
#

our($JSON, @Mandatory, %IdFields);

$JSON = JSON->new()->utf8(1);

@Mandatory = qw(type name group ts);

# primary
$IdFields{broker}   = [ qw(broker) ];
$IdFields{client}   = [ qw(client) ];
$IdFields{host}     = [ qw(host) ];
$IdFields{path}     = [ qw(path) ];
$IdFields{hostpath} = [ qw(host path) ];
$IdFields{msgbrk}   = [ qw(host broker) ];
$IdFields{msgclt}   = [ qw(host broker client) ];
$IdFields{msgsvc}   = [ qw(host broker proto port) ];
$IdFields{brkclt}   = [ qw(broker client) ];
$IdFields{brksvc}   = [ qw(broker proto port) ];
$IdFields{service}  = [ qw(host proto port) ];
# secondary
$IdFields{df}       = [ qw(host path) ];
$IdFields{dirq}     = [ qw(host path) ];
$IdFields{io}       = [ qw(host path) ];
$IdFields{metis}    = [ qw(host path) ];
$IdFields{zfs}      = [ qw(host) ];
# check metrics
$IdFields{msgchk}   = [ qw(host broker proto port check) ];
$IdFields{svcchk}   = [ qw(host proto port check) ];

#
# validate one event
#

sub _validate_mandatory ($) {
    my($event) = @_;
    my(@fields);

    # per-type mandatory fields
    if ($event->{type} eq "check") {
        @fields = @MBM::Event::Check::Mandatory;
    } elsif ($event->{type} eq "log") {
        @fields = @MBM::Event::Log::Mandatory;
    } elsif ($event->{type} eq "metric") {
        @fields = @MBM::Event::Metric::Mandatory;
    } elsif ($event->{type} eq "status") {
        @fields = @MBM::Event::Status::Mandatory;
    } else {
        dief("invalid event (invalid type): %s", $JSON->encode($event));
    }
    foreach my $field (@fields) {
        dief("invalid %s event (missing %s field): %s",
             $event->{type}, $field, $JSON->encode($event))
            unless defined($event->{$field});
    }
}

sub _validate_enumeration ($) {
    my($event) = @_;

    # per-type enumeration fields
    if ($event->{type} eq "check") {
        dief("invalid check event (invalid value): %s", $JSON->encode($event))
            unless $event->{value} =~ /^\d+$/
               and $MBM::Event::Check::Val2Str[$event->{value}];
    } elsif ($event->{type} eq "log" and defined($event->{severity})) {
        dief("invalid log event (invalid severity): %s", $JSON->encode($event))
            unless $event->{severity} =~ /^\d+$/
               and $MBM::Event::Log::Severity[$event->{severity}];
    } elsif ($event->{type} eq "status") {
        dief("invalid status event (invalid value): %s", $JSON->encode($event))
            unless $event->{value} =~ /^\d+$/
               and $MBM::Event::Status::Val2Str[$event->{value}];
    }
}

sub validate ($) {
    my($event) = @_;

    # sanity
    dief("invalid event: %s", $JSON->encode($event))
        unless ref($event) eq "HASH";
    # mandatory fields
    foreach my $field (@Mandatory) {
        dief("invalid event (missing %s field): %s",
             $field, $JSON->encode($event)) unless $event->{$field};
    }
    # ts is always numeric!
    $event->{ts} += 0 if $event->{ts} =~ /[eE][\+\-]?\d+$/;
    # field names and values
    while (my($field, $value) = each(%{ $event })) {
        goto invalid unless $field =~ /^[a-z0-9_]+$/i;
        goto invalid unless defined($value);
        next unless ref($value);
        next if JSON::is_bool($value);
        next if ref($value) eq "HASH" and $field eq "extra";
      invalid:
        dief("invalid event (invalid %s field): %s",
             $field, $JSON->encode($event));
    }
    # advanced validations
    _validate_mandatory($event);
    _validate_enumeration($event);
}

#
# return the "family id" of the event
#

sub fid ($) {
    my($event) = @_;
    my(@list);

    foreach my $field (qw(type group name)) {
        push(@list, $event->{$field});
    }
    # log events always have a path!
    push(@list, $event->{path}) if $event->{type} eq "log";
    foreach my $field (@{ $IdFields{$event->{group}} }) {
        push(@list, $event->{$field});
    }
    return(join(":", @list));
}

#
# create code to filter events
#

sub _filter_test ($$) {
    my($field, $value) = @_;
    my($code);

    $code = "defined(\$_e->{$field}) and \$_e->{$field} ";
    $code .= $value =~ /^\d+$/ ? "== $value" : "eq '$value'";
    return($code);
}

sub _filter_set ($$) {
    my($field, $value) = @_;
    my($code);

    $code = "\$_e->{$field} = ";
    $code .= $value =~ /^\d+$/ ? $value : "'$value'";
    return($code);
}

sub filter ($) {
    my($filter) = @_;
    my($t, $code, $pre, $post, @list, $sub);

    $t = "    ";
    $code = "sub (\$) {\n";
    $code .= "${t}my(\$_e) = \@_;\n";
    foreach my $line (split(/\n/, $filter)) {
        next if $line =~ /^\s*\#/ or $line =~ /^\s*$/;
        if ($line =~ /^\s*(.+?)\s+=>\s+(.+?)\s*$/) {
            ($pre, $post) = ($1, $2);
            @list = ();
            foreach my $cond (split(/\s+/, $pre)) {
                if ($cond =~ /^(\w+)=(\w+)$/) {
                    push(@list, _filter_test($1, $2));
                } else {
                    dief("unexpected filter condition: %s", $cond);
                }
            }
            dief("unexpected filter line: %s", $line) unless @list;
            $code .= "${t}if (" . join(" and\n${t}${t}", @list) . ") {\n";
            if ($post eq "ignore") {
                $code .= "${t}${t}return();\n";
            } else {
                foreach my $action (split(/\s+/, $post)) {
                    if ($action =~ /^(\w+)=(\w+)$/) {
                        $code .= $t . $t . _filter_set($1, $2) . ";\n";
                    } else {
                        dief("unexpected filter action: %s", $action);
                    }
                }
            }
            $code .= "${t}}\n";
        } else {
            dief("unexpected filter line: %s", $line);
        }
    }
    $code .= "${t}return(\$_e);\n";
    $code .= "}\n";
    $sub = eval($code); ## no critic 'ProhibitStringyEval'
    dief("invalid filter: %s", $@) if $@;
    return($sub);
}

#+##############################################################################
#                                                                              #
# MBM::Event::Check sub-module                                                 #
#                                                                              #
#-##############################################################################

#
# module definition
#

package MBM::Event::Check;
use strict;
use warnings;

#
# constants
#

use constant VAL_OK       => 0;
use constant VAL_WARNING  => 1;
use constant VAL_CRITICAL => 2;
use constant VAL_UNKNOWN  => 3;

#
# global variables
#

our(@Mandatory, @Val2Str); ## no critic 'Variables::ProhibitReusedName'

@Mandatory = qw(value);

$Val2Str[VAL_OK]       = "OK";
$Val2Str[VAL_WARNING]  = "WARNING";
$Val2Str[VAL_CRITICAL] = "CRITICAL";
$Val2Str[VAL_UNKNOWN]  = "UNKNOWN";

#+##############################################################################
#                                                                              #
# MBM::Event::Log sub-module                                                   #
#                                                                              #
#-##############################################################################

#
# module definition
#

package MBM::Event::Log;
use strict;
use warnings;

#
# constants
#

use constant SEVERITY_UNKNOWN => 0;
use constant SEVERITY_DEBUG   => 1;
use constant SEVERITY_INFO    => 2;
use constant SEVERITY_WARNING => 3;
use constant SEVERITY_ERROR   => 4;

#
# global variables
#

our(@Mandatory, @Severity); ## no critic 'Variables::ProhibitReusedName'

@Mandatory = qw(path text);

$Severity[SEVERITY_UNKNOWN] = "UNKNOWN";
$Severity[SEVERITY_DEBUG]   = "DEBUG";
$Severity[SEVERITY_INFO]    = "INFO";
$Severity[SEVERITY_WARNING] = "WARNING";
$Severity[SEVERITY_ERROR]   = "ERROR";

#+##############################################################################
#                                                                              #
# MBM::Event::Metric sub-module                                                #
#                                                                              #
#-##############################################################################

#
# module definition
#

package MBM::Event::Metric;
use strict;
use warnings;

#
# global variables
#

our(@Mandatory); ## no critic 'Variables::ProhibitReusedName'

@Mandatory = qw(value);


#+##############################################################################
#                                                                              #
# MBM::Event::Status sub-module                                                #
#                                                                              #
#-##############################################################################

#
# module definition
#

package MBM::Event::Status;
use strict;
use warnings;

#
# constants
#

use constant VAL_OK       => 0;
use constant VAL_WARNING  => 1;
use constant VAL_CRITICAL => 2;
use constant VAL_UNKNOWN  => 3;

#
# global variables
#

our(@Mandatory, @Val2Str); ## no critic 'Variables::ProhibitReusedName'

@Mandatory = qw(value);

$Val2Str[VAL_OK]       = "OK";
$Val2Str[VAL_WARNING]  = "WARNING";
$Val2Str[VAL_CRITICAL] = "CRITICAL";
$Val2Str[VAL_UNKNOWN]  = "UNKNOWN";

1;
